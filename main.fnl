(require-macros :helpers)
(require-macros :formspec)

(local fsheader "formspec_version[3]")

(fn show-train-remote-formspec [pname trainid]
  `(let [pn# ,pname tid# ,trainid]
     (if (not (check-train-access pn# tid#)) nil
         (let [t# (. advtrains.trains tid#)]
           (local fs# (formspec
                       ,fsheader
                       [:size [11.5 11]]
                       [:label [0.5 0.75] (S "Remote control for @1" tid#)]
                       [:label [0.5 1.25] (S "Wagon(s): @1" (length t#.trainparts))]
                       [:button [6 0.5] [4 1] :update (S "Update")]
                       [:button_exit [10 0.5] [1 1] "close-formspec" "X"]
                       [:label [1.25 2] (S "Accelerate")]
                       [:label [1.25 2.5] (S "Neutral")]
                       [:label [1.25 3] (S "Roll")]
                       [:label [1.25 3.5] (S "Regular brake")]
                       [:label [1.25 4] (S "Emergency brake")]
                       [:scrollbaroptions
                        "min=1" "max=5"
                        "smallstep=1" "largestep=1"
                        "thumbsize=1" "arrows=hide"]
                       [:scrollbar [0.5 1.75] [0.5 2.5] :vertical :lever (- 5 (leverof t#))]
                       [:label [6 2] (S "Door control")]
                       [:scrollbaroptions
                        "min=1" "max=3"
                        "smallstep=1" "largestep=1"
                        "thumbsize=1" "arrows=hide"]
                       [:scrollbar [8.5 1.75] [2.5 0.5] :horizontal :door (+ (or t#.door_open 0) 2)]
                       [:checkbox [6 2.75] :ars (S "Automatic routesetting")
                        (if (and advtrains.interlocking t#.ars_disable) "false" "true")]
                       [:button [6 3.25] [5 1] :reverse (S "Reverse train")]
                       [:field [0.5 5] [9.5 1] :outtext (S "External display")
                        (minetest.formspec_escape (or t#.text_outside ""))]
                       [:button [10 5] [1 1] :outset (S "Set")]
                       [:field [0.5 6.5] [9.5 1] :intext (S "Internal display")
                        (minetest.formspec_escape (or t#.text_inside ""))]
                       [:button [10 6.5] [1 1] :inset (S "Set")]
                       [:field [0.5 8] [9.5 1] :lntext (S "Line")
                        (minetest.formspec_escape (or t#.line ""))]
                       [:button [10 8] [1 1] :lnset (S "Set")]
                       [:field [0.5 9.5] [9.5 1] :rctext (S "Routing code")
                        (minetest.formspec_escape (or t#.routingcode ""))]
                       [:button [10 9.5] [1 1] :rcset (S "Set")]
                       ))
           (minetest.show_formspec pn# (.. "train_remote:t" tid#) fs#)))))

(fn handle-train-control-fields [trainid pname fields]
  `(let [trainid# ,trainid pname# ,pname fields# ,fields]
     (if (not trainid#) nil
         (not (check-train-access pname# trainid#)) nil
         fields#.close-formspec nil
         (minetest.is_yes fields#.quit) nil
         (let [t# (. advtrains.trains trainid#)]
           (if (has-scrollbar-event fields#.door)
               (let [dside# (get-scrollbar fields#.door 1 3)]
                 (if dside# (tset t# :door_open (- dside# 2))))
               (has-scrollbar-event fields#.lever)
               (let [tlev# (get-scrollbar fields#.lever 1 5)]
                 (if tlev# (tset t# :ctrl_user (- 5 tlev#))))
               fields#.reverse
               (when (<= t#.velocity 0)
                 (advtrains.invert_train trainid#)
                 (advtrains.atc.train_reset_command t#))
               fields#.ars
               (if advtrains.interlocking
                   (advtrains.interlocking.ars_set_disable
                    t# (not (minetest.is_yes fields#.ars)))))
           (when fields#.outtext
             (tset t# :text_outside fields#.outtext))
           (when fields#.intext
             (tset t# :text_inside fields#.intext))
           (when fields#.lntext
             (tset t# :line fields#.lntext)
             (minetest.after 0 advtrains.invalidate_path trainid#))
           (when fields#.rctext
             (tset t# :routingcode fields#.rctext)
             (minetest.after 0 advtrains.invalidate_path trainid#))
           (show-train-remote-formspec pname# trainid#)
           nil))))

;; Stub
(fn handle-wagon-control-fields [wagonid pname fields]
  `(let [wagonid# ,wagonid pname# ,pname fields# ,fields] nil))

{: show-train-remote-formspec
 : handle-train-control-fields
 : handle-wagon-control-fields
 }
