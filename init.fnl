(require-macros :main)
(require-macros :helpers)
(require-macros :formspec)

(minetest.register_on_player_receive_fields
 (fn [player formname fields]
   (let [pname
         (player:get_player_name)
         trainid
         (string.match formname "^train_remote:t(%S+)$")
         wagonid
         (string.match formname "^train_remote:w(%S+)$")]
     (if trainid (handle-train-control-fields trainid pname fields)
         wagonid (handle-wagon-control-fields wagonid pname fields)
         nil))))

(minetest.register_chatcommand
 "train_remote"
 { :params
   "<ID>"
   :description
   (S "Remotely control the train with the given ID")
   :func
   (fn [pname trainid]
     (let [(accessp msg) (check-train-access pname trainid)]
       (if (not accessp) (values false msg)
           (do (show-train-remote-formspec pname trainid) nil))))})
