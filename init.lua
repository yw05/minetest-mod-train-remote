local function _1_(player, formname, fields)
  local pname = player:get_player_name()
  local trainid = string.match(formname, "^train_remote:t(%S+)$")
  local wagonid = string.match(formname, "^train_remote:w(%S+)$")
  if trainid then
    local trainid_5_auto = trainid
    local pname_6_auto = pname
    local fields_7_auto = fields
    if not trainid_5_auto then
      return nil
    else
      local _2_
      do
        local pn_3_auto = pname_6_auto
        local tid_4_auto = trainid_5_auto
        if not minetest.get_player_by_name(pn_3_auto) then
          minetest.translate("train_remote", "Player is not online")
          _2_ = false
        else
          local t_5_auto = advtrains.trains[tid_4_auto]
          if not t_5_auto then
            minetest.translate("train_remote", "No train with ID @1", tid_4_auto)
            _2_ = false
          else
            local tps_6_auto = t_5_auto.trainparts
            local n_7_auto = #tps_6_auto
            local wagons_8_auto = advtrains.wagons
            local i_9_auto = 0
            local contp_10_auto = true
            while ((i_9_auto <= n_7_auto) and contp_10_auto) do
              local w_11_auto = (wagons_8_auto)[(tps_6_auto)[i_9_auto]]
              local function _5_()
                local w_2_auto = w_11_auto
                return advtrains.check_driving_couple_protection(pname, w_2_auto.owner, w_2_auto.whitelist)
              end
              if (w_11_auto and _5_()) then
                contp_10_auto = false
              else
                i_9_auto = (1 + i_9_auto)
              end
            end
            _2_ = not contp_10_auto
          end
        end
      end
      if not _2_ then
        return nil
      elseif fields_7_auto["close-formspec"] then
        return nil
      elseif minetest.is_yes(fields_7_auto.quit) then
        return nil
      else
        local t_8_auto = advtrains.trains[trainid_5_auto]
        local _9_
        do
          local et_14_auto = minetest.explode_scrollbar_event(fields_7_auto.door)
          if not et_14_auto then
            _9_ = false
          else
            _9_ = (et_14_auto.type == "CHG")
          end
        end
        if _9_ then
          local dside_9_auto
          do
            local et_15_auto = minetest.explode_scrollbar_event(fields_7_auto.door)
            if not et_15_auto then
              dside_9_auto = nil
            elseif ((et_15_auto.type == "CHG") and (et_15_auto.value >= 1) and (et_15_auto.value <= 3)) then
              dside_9_auto = et_15_auto.value
            else
              dside_9_auto = nil
            end
          end
          if dside_9_auto then
            t_8_auto["door_open"] = (dside_9_auto - 2)
          end
        else
          local _13_
          do
            local et_14_auto = minetest.explode_scrollbar_event(fields_7_auto.lever)
            if not et_14_auto then
              _13_ = false
            else
              _13_ = (et_14_auto.type == "CHG")
            end
          end
          if _13_ then
            local tlev_10_auto
            do
              local et_15_auto = minetest.explode_scrollbar_event(fields_7_auto.lever)
              if not et_15_auto then
                tlev_10_auto = nil
              elseif ((et_15_auto.type == "CHG") and (et_15_auto.value >= 1) and (et_15_auto.value <= 5)) then
                tlev_10_auto = et_15_auto.value
              else
                tlev_10_auto = nil
              end
            end
            if tlev_10_auto then
              t_8_auto["ctrl_user"] = (5 - tlev_10_auto)
            end
          elseif fields_7_auto.reverse then
            if (t_8_auto.velocity <= 0) then
              advtrains.invert_train(trainid_5_auto)
              advtrains.atc.train_reset_command(t_8_auto)
            end
          elseif fields_7_auto.ars then
            if advtrains.interlocking then
              advtrains.interlocking.ars_set_disable(t_8_auto, not minetest.is_yes(fields_7_auto.ars))
            end
          end
        end
        if fields_7_auto.outtext then
          t_8_auto["text_outside"] = fields_7_auto.outtext
        end
        if fields_7_auto.intext then
          t_8_auto["text_inside"] = fields_7_auto.intext
        end
        if fields_7_auto.lntext then
          t_8_auto["line"] = fields_7_auto.lntext
          minetest.after(0, advtrains.invalidate_path, trainid_5_auto)
        end
        if fields_7_auto.rctext then
          t_8_auto["routingcode"] = fields_7_auto.rctext
          minetest.after(0, advtrains.invalidate_path, trainid_5_auto)
        end
        do
          local pn_1_auto = pname_6_auto
          local tid_2_auto = trainid_5_auto
          local _24_
          do
            local pn_3_auto = pn_1_auto
            local tid_4_auto = tid_2_auto
            if not minetest.get_player_by_name(pn_3_auto) then
              minetest.translate("train_remote", "Player is not online")
              _24_ = false
            else
              local t_5_auto = advtrains.trains[tid_4_auto]
              if not t_5_auto then
                minetest.translate("train_remote", "No train with ID @1", tid_4_auto)
                _24_ = false
              else
                local tps_6_auto = t_5_auto.trainparts
                local n_7_auto = #tps_6_auto
                local wagons_8_auto = advtrains.wagons
                local i_9_auto = 0
                local contp_10_auto = true
                while ((i_9_auto <= n_7_auto) and contp_10_auto) do
                  local w_11_auto = (wagons_8_auto)[(tps_6_auto)[i_9_auto]]
                  local function _27_()
                    local w_2_auto = w_11_auto
                    return advtrains.check_driving_couple_protection(pname, w_2_auto.owner, w_2_auto.whitelist)
                  end
                  if (w_11_auto and _27_()) then
                    contp_10_auto = false
                  else
                    i_9_auto = (1 + i_9_auto)
                  end
                end
                _24_ = not contp_10_auto
              end
            end
          end
          if not _24_ then
          else
            local t_3_auto = advtrains.trains[tid_2_auto]
            local fs_4_auto
            local _31_
            do
              local t_12_auto = t_3_auto
              if not t_12_auto then
                _31_ = nil
              elseif ((t_12_auto.velocity == 0) and not t_12_auto.active_control) then
                _31_ = 1
              elseif t_12_auto.hud_lzb_effect_tmr then
                _31_ = 1
              else
                _31_ = (t_12_auto.lever or 3)
              end
            end
            local function _33_()
              if (advtrains.interlocking and t_3_auto.ars_disable) then
                return "false"
              else
                return "true"
              end
            end
            fs_4_auto = table.concat({"formspec_version[3]", string.format("%s[%s]", "size", "11.5,11"), string.format("%s[%s]", "label", table.concat({"0.5,0.75", tostring(minetest.translate("train_remote", "Remote control for @1", tid_2_auto))}, ";")), string.format("%s[%s]", "label", table.concat({"0.5,1.25", tostring(minetest.translate("train_remote", "Wagon(s): @1", #t_3_auto.trainparts))}, ";")), string.format("%s[%s]", "button", table.concat({"6,0.5;4,1;update", tostring(minetest.translate("train_remote", "Update"))}, ";")), string.format("%s[%s]", "button_exit", "10,0.5;1,1;close-formspec;X"), string.format("%s[%s]", "label", table.concat({"1.25,2", tostring(minetest.translate("train_remote", "Accelerate"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,2.5", tostring(minetest.translate("train_remote", "Neutral"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,3", tostring(minetest.translate("train_remote", "Roll"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,3.5", tostring(minetest.translate("train_remote", "Regular brake"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,4", tostring(minetest.translate("train_remote", "Emergency brake"))}, ";")), string.format("%s[%s]", "scrollbaroptions", "min=1;max=5;smallstep=1;largestep=1;thumbsize=1;arrows=hide"), string.format("%s[%s]", "scrollbar", table.concat({"0.5,1.75;0.5,2.5;vertical;lever", tostring((5 - _31_))}, ";")), string.format("%s[%s]", "label", table.concat({"6,2", tostring(minetest.translate("train_remote", "Door control"))}, ";")), string.format("%s[%s]", "scrollbaroptions", "min=1;max=3;smallstep=1;largestep=1;thumbsize=1;arrows=hide"), string.format("%s[%s]", "scrollbar", table.concat({"8.5,1.75;2.5,0.5;horizontal;door", tostring(((t_3_auto.door_open or 0) + 2))}, ";")), string.format("%s[%s]", "checkbox", table.concat({"6,2.75;ars", tostring(minetest.translate("train_remote", "Automatic routesetting")), tostring(_33_())}, ";")), string.format("%s[%s]", "button", table.concat({"6,3.25;5,1;reverse", tostring(minetest.translate("train_remote", "Reverse train"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,5;9.5,1;outtext", tostring(minetest.translate("train_remote", "External display")), tostring(minetest.formspec_escape((t_3_auto.text_outside or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,5;1,1;outset", tostring(minetest.translate("train_remote", "Set"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,6.5;9.5,1;intext", tostring(minetest.translate("train_remote", "Internal display")), tostring(minetest.formspec_escape((t_3_auto.text_inside or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,6.5;1,1;inset", tostring(minetest.translate("train_remote", "Set"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,8;9.5,1;lntext", tostring(minetest.translate("train_remote", "Line")), tostring(minetest.formspec_escape((t_3_auto.line or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,8;1,1;lnset", tostring(minetest.translate("train_remote", "Set"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,9.5;9.5,1;rctext", tostring(minetest.translate("train_remote", "Routing code")), tostring(minetest.formspec_escape((t_3_auto.routingcode or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,9.5;1,1;rcset", tostring(minetest.translate("train_remote", "Set"))}, ";"))}, " ")
            minetest.show_formspec(pn_1_auto, ("train_remote:t" .. tid_2_auto), fs_4_auto)
          end
        end
        return nil
      end
    end
  elseif wagonid then
    local wagonid_11_auto = wagonid
    local pname_12_auto = pname
    local fields_13_auto = fields
    return nil
  else
    return nil
  end
end
minetest.register_on_player_receive_fields(_1_)
local function _37_(pname, trainid)
  local accessp, msg = nil, nil
  do
    local pn_3_auto = pname
    local tid_4_auto = trainid
    if not minetest.get_player_by_name(pn_3_auto) then
      accessp, msg = false, minetest.translate("train_remote", "Player is not online")
    else
      local t_5_auto = advtrains.trains[tid_4_auto]
      if not t_5_auto then
        accessp, msg = false, minetest.translate("train_remote", "No train with ID @1", tid_4_auto)
      else
        local tps_6_auto = t_5_auto.trainparts
        local n_7_auto = #tps_6_auto
        local wagons_8_auto = advtrains.wagons
        local i_9_auto = 0
        local contp_10_auto = true
        while ((i_9_auto <= n_7_auto) and contp_10_auto) do
          local w_11_auto = (wagons_8_auto)[(tps_6_auto)[i_9_auto]]
          local function _38_()
            local w_2_auto = w_11_auto
            return advtrains.check_driving_couple_protection(pname, w_2_auto.owner, w_2_auto.whitelist)
          end
          if (w_11_auto and _38_()) then
            contp_10_auto = false
          else
            i_9_auto = (1 + i_9_auto)
          end
        end
        accessp, msg = not contp_10_auto
      end
    end
  end
  if not accessp then
    return false, msg
  else
    do
      local pn_1_auto = pname
      local tid_2_auto = trainid
      local _42_
      do
        local pn_3_auto = pn_1_auto
        local tid_4_auto = tid_2_auto
        if not minetest.get_player_by_name(pn_3_auto) then
          minetest.translate("train_remote", "Player is not online")
          _42_ = false
        else
          local t_5_auto = advtrains.trains[tid_4_auto]
          if not t_5_auto then
            minetest.translate("train_remote", "No train with ID @1", tid_4_auto)
            _42_ = false
          else
            local tps_6_auto = t_5_auto.trainparts
            local n_7_auto = #tps_6_auto
            local wagons_8_auto = advtrains.wagons
            local i_9_auto = 0
            local contp_10_auto = true
            while ((i_9_auto <= n_7_auto) and contp_10_auto) do
              local w_11_auto = (wagons_8_auto)[(tps_6_auto)[i_9_auto]]
              local function _45_()
                local w_2_auto = w_11_auto
                return advtrains.check_driving_couple_protection(pname, w_2_auto.owner, w_2_auto.whitelist)
              end
              if (w_11_auto and _45_()) then
                contp_10_auto = false
              else
                i_9_auto = (1 + i_9_auto)
              end
            end
            _42_ = not contp_10_auto
          end
        end
      end
      if not _42_ then
      else
        local t_3_auto = advtrains.trains[tid_2_auto]
        local fs_4_auto
        local _49_
        do
          local t_12_auto = t_3_auto
          if not t_12_auto then
            _49_ = nil
          elseif ((t_12_auto.velocity == 0) and not t_12_auto.active_control) then
            _49_ = 1
          elseif t_12_auto.hud_lzb_effect_tmr then
            _49_ = 1
          else
            _49_ = (t_12_auto.lever or 3)
          end
        end
        local function _51_()
          if (advtrains.interlocking and t_3_auto.ars_disable) then
            return "false"
          else
            return "true"
          end
        end
        fs_4_auto = table.concat({"formspec_version[3]", string.format("%s[%s]", "size", "11.5,11"), string.format("%s[%s]", "label", table.concat({"0.5,0.75", tostring(minetest.translate("train_remote", "Remote control for @1", tid_2_auto))}, ";")), string.format("%s[%s]", "label", table.concat({"0.5,1.25", tostring(minetest.translate("train_remote", "Wagon(s): @1", #t_3_auto.trainparts))}, ";")), string.format("%s[%s]", "button", table.concat({"6,0.5;4,1;update", tostring(minetest.translate("train_remote", "Update"))}, ";")), string.format("%s[%s]", "button_exit", "10,0.5;1,1;close-formspec;X"), string.format("%s[%s]", "label", table.concat({"1.25,2", tostring(minetest.translate("train_remote", "Accelerate"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,2.5", tostring(minetest.translate("train_remote", "Neutral"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,3", tostring(minetest.translate("train_remote", "Roll"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,3.5", tostring(minetest.translate("train_remote", "Regular brake"))}, ";")), string.format("%s[%s]", "label", table.concat({"1.25,4", tostring(minetest.translate("train_remote", "Emergency brake"))}, ";")), string.format("%s[%s]", "scrollbaroptions", "min=1;max=5;smallstep=1;largestep=1;thumbsize=1;arrows=hide"), string.format("%s[%s]", "scrollbar", table.concat({"0.5,1.75;0.5,2.5;vertical;lever", tostring((5 - _49_))}, ";")), string.format("%s[%s]", "label", table.concat({"6,2", tostring(minetest.translate("train_remote", "Door control"))}, ";")), string.format("%s[%s]", "scrollbaroptions", "min=1;max=3;smallstep=1;largestep=1;thumbsize=1;arrows=hide"), string.format("%s[%s]", "scrollbar", table.concat({"8.5,1.75;2.5,0.5;horizontal;door", tostring(((t_3_auto.door_open or 0) + 2))}, ";")), string.format("%s[%s]", "checkbox", table.concat({"6,2.75;ars", tostring(minetest.translate("train_remote", "Automatic routesetting")), tostring(_51_())}, ";")), string.format("%s[%s]", "button", table.concat({"6,3.25;5,1;reverse", tostring(minetest.translate("train_remote", "Reverse train"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,5;9.5,1;outtext", tostring(minetest.translate("train_remote", "External display")), tostring(minetest.formspec_escape((t_3_auto.text_outside or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,5;1,1;outset", tostring(minetest.translate("train_remote", "Set"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,6.5;9.5,1;intext", tostring(minetest.translate("train_remote", "Internal display")), tostring(minetest.formspec_escape((t_3_auto.text_inside or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,6.5;1,1;inset", tostring(minetest.translate("train_remote", "Set"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,8;9.5,1;lntext", tostring(minetest.translate("train_remote", "Line")), tostring(minetest.formspec_escape((t_3_auto.line or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,8;1,1;lnset", tostring(minetest.translate("train_remote", "Set"))}, ";")), string.format("%s[%s]", "field", table.concat({"0.5,9.5;9.5,1;rctext", tostring(minetest.translate("train_remote", "Routing code")), tostring(minetest.formspec_escape((t_3_auto.routingcode or "")))}, ";")), string.format("%s[%s]", "button", table.concat({"10,9.5;1,1;rcset", tostring(minetest.translate("train_remote", "Set"))}, ";"))}, " ")
        minetest.show_formspec(pn_1_auto, ("train_remote:t" .. tid_2_auto), fs_4_auto)
      end
    end
    return nil
  end
end
return minetest.register_chatcommand("train_remote", {description = minetest.translate("train_remote", "Remotely control the train with the given ID"), func = _37_, params = "<ID>"})
