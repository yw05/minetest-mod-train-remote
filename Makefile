# trivial makefile to generate init.lua
all: init.lua

%.lua: %.fnl
	fennel --compile $< > $@

.PHONY: test clean

test: macrotest.fnl
	fennel macrotest.fnl

clean:
	-rm *.lua *~
